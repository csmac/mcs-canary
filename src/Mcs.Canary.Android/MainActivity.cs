﻿using Android.App;
using Android.OS;
using Android.Widget;
using Mcs.Canary.Core.ApiAccess;
using Mcs.Canary.Core.Models;
using System;

namespace Mcs.Canary.Android
{
    [Activity(Label = "Mcs.Canary.Android", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : Activity
    {
        private readonly IApiClient _client;

        public MainActivity()
        {
            _client = new PortalApiClient(_jwt);
        }

        private string _jwt = "hard-coded-jwt-here";

        public Button RetrieveIdentityButton
        {
            get
            {
                return FindViewById<Button>(Resource.Id.RetrieveIdentityButton);
            }
        }

        public TextView NameLabel
        {
            get
            {
                return FindViewById<TextView>(Resource.Id.NameLabel);
            }
        }

        public TextView CompanyLabel
        {
            get
            {
                return FindViewById<TextView>(Resource.Id.CompanyLabel);
            }
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            RetrieveIdentityButton.Click += RetrieveIdentityButton_Click;
        }

        private async void RetrieveIdentityButton_Click(object sender, EventArgs e)
        {
            var response = await _client.GetAsync<SingleDataResponse<Identity>>("https://lab-api.macquariecloudservices.com/identity");

            if (response.IsSuccessful)
            {
                NameLabel.Text = $"{response.Content.FirstName} {response.Content.LastName}";
                CompanyLabel.Text = response.Content.CompanyName;
            }
            else
            {
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.SetTitle("Oops, I did it again!");
                alert.SetMessage(response.Message);
                alert.SetNegativeButton("Close", (senderAlert, args) => {
                    Toast.MakeText(this, "Closed!", ToastLength.Short).Show();
                });
                Dialog dialog = alert.Create();
                dialog.Show();
            }
        }
    }
}

