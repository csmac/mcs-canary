﻿using System.Collections.Generic;

namespace Mcs.Canary.Core.Models
{
    public class DataResponseBase
    {
        public int Status { get; set; }

        public string Message { get; set; }

        public IEnumerable<ErrorLogs> Errors { get; set; }

        public bool IsSuccessful
        {
            get
            {
                return Status < 400;
            }
        }
    }
}