﻿namespace Mcs.Canary.Core.Models
{
    public class SingleDataResponse<T> : DataResponseBase
    {
        public T Content { get; set; }
    }
}