﻿namespace Mcs.Canary.Core.Models
{
    public class ErrorLogs
    {
        public string Code { get; set; }

        public string Message { get; set; }
    }
}