﻿namespace Mcs.Canary.Core.Models
{
    public class MultiDataResponse<T> : DataResponseBase
    {
        public int TotalCount { get; set; }

        public T Content { get; set; }
    }
}