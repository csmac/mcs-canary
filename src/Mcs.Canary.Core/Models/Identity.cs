﻿using System;
using System.Collections.Generic;

namespace Mcs.Canary.Core.Models
{
    public class Identity
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string UserId { get; set; }

        public string Email { get; set; }

        public string CompanyId { get; set; }

        public string CompanyName { get; set; }

        public DateTime Expiry { get; set; }

        public IEnumerable<string> Permissions { get; set; }
    }
}