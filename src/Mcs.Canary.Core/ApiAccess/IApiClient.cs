﻿using System.Threading.Tasks;

namespace Mcs.Canary.Core.ApiAccess
{
    public interface IApiClient
    {
        Task<T> GetAsync<T>(string route);
    }
}