﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Mcs.Canary.Core.ApiAccess
{
    public class PortalApiClient : IApiClient
    {
        private readonly HttpClient _client;

        public PortalApiClient(string jwt)
        {
            _client = new HttpClient();
            SetupRequestHeaders(jwt);
        }

        public async Task<T> GetAsync<T>(string route)
        {
            var response = await _client.GetAsync(route);
            var jsonString = await response.Content.ReadAsStringAsync();
            await Task.Delay(1);
            var content = JsonConvert.DeserializeObject<T>(jsonString);
            
            return content;
        }

        private void SetupRequestHeaders(string jwt)
        {
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", jwt);
        }
    }
}